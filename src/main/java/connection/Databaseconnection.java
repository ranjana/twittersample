package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Databaseconnection {
	
	private static Databaseconnection INSTANCE ;
	private static Connection con = null;
	
	public static Connection getInstance() {
		if(INSTANCE == null || INSTANCE.con == null) {
			INSTANCE = new Databaseconnection();
		}
		
		return INSTANCE.con;

	}
	
	private Databaseconnection() {
		String url = "jdbc:mysql://localhost:3306/twitter_sample?allowPublicKeyRetrieval=true&useSSL=false";
		String username = "root";
		String pass = "";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
//			Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection(url,username,pass);
				System.out.println("data base sonnection successful");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage()+"Databaseconnection exception");

			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{ 
            System.out.println("finally block executed"); 
        }
	}

	
}
