package datatransferobject;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import DBHelper.LoginDataBase;
import requestresponse.LoginResponse;


public class ProfileDTO{
	public   String userid;
	public  String firstName;
	public   String lastName;
	public   int age;
	public  String profileurl;
	public  long followers;
	public  long following;
	
	 public ProfileDTO() {
	
	 }
    public ProfileDTO(Builder profile) {
    	this.userid = profile.userid;
		this.age = profile.age;
		this.firstName = profile.firstName;
		this.lastName = profile.lastName;
		this.profileurl = profile.profileurl;
		this.followers = profile.followers;
		this.following = profile.following;	
	}
    
    
    public String getUserid() {
		return userid;
	}


	public String getFirstName() {
		return firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public int getAge() {
		return age;
	}


	public String getProfileurl() {
		return profileurl;
	}


	public long getFollowers() {
		return followers;
	}


	public long getFollowing() {
		return following;
	}


	public static class Builder{
		public String userid;
		public String firstName;
		public String lastName;
		public int age;
		public String profileurl;
		public long followers;
		public long following;
    	
    	
    	public Builder userId(String userid) {
    		this.userid = userid;
    		return this;
    	}
    	
public Builder firstName(String firstName) {
	this.firstName = firstName;
	return this;
    	}
public Builder lastName(String lastName) {
	this.lastName = lastName;
	return this;
}
public Builder profileUrl(String profileurl) {
	this.profileurl = profileurl;
	return this;
	
}
public Builder age(int age) {
	this.age = age;
	return this;
	
}
public Builder followers(long followers) {
	this.followers = followers;
	return this;
}
public Builder following(long following) {
	this.following = following;
	return this;
}
    
public ProfileDTO build() {
    ProfileDTO profiledto =  new ProfileDTO(this);
    return profiledto;
}	
    }
	
//	public static class CustomDeserilizer extends JsonDeserializer<ProfileDTO> {
//        @Override
//        public ProfileDTO deserialize(JsonParser jsonParser, com.fasterxml.jackson.databind.DeserializationContext ctxt)
//                throws IOException {
//            final ProfileDTO instance = new ProfileDTO();
//           
//            if (jsonParser.getCurrentToken() == null) {
//                jsonParser.nextToken();
//            }
//            if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
//                jsonParser.skipChildren();
//                return null;
//            }
//            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
//                String fieldName = jsonParser.getCurrentName();
//                jsonParser.nextToken();
//                instance.parseField(instance, fieldName, jsonParser);
//                jsonParser.skipChildren();
//            }
//            return instance;
//           }
//	}

//		public void parseField(ProfileDTO instance, String fieldName, JsonParser p)  throws IOException{
			// TODO Auto-generated method stub
//			 switch (fieldName) {
//			 case "userid":
//	                p.nextToken();
//	                userid = p.readValueAs(String.class);
//	                break;
//	            case "firstName":
//	                p.nextToken();
//	                firstName = p.readValueAs(String.class);
//	                break;
//	            case "lastName":
//	                p.nextToken();
//	                lastName = p.readValueAs(String.class);
//	                break;
//	            case "age":
//	                p.nextToken();
//	                age = p.readValueAs(Integer.class);
//	                break;
//	            case "profileurl":
//	                p.nextToken();
//	                profileurl = p.readValueAs(String.class);
//	                break;
//	            case "followers":
//	                p.nextToken();
//	                followers = p.readValueAs(Integer.class);
//	                break;
//	            case "following":
//	                p.nextToken();
//	                following = p.readValueAs(Integer.class);
//	                break;
//	          
//	        }
//			
//		}
		public class IntervalSerializer extends JsonSerializer {
		@Override
		public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			// TODO Auto-generated method stub
			jgen.writeStartObject();
	        jgen.writeStringField("userid", userid);
	        jgen.writeStringField("firstName", firstName);
	        jgen.writeStringField("lastName", lastName);
	        jgen.writeNumberField("age", age);
	        jgen.writeStringField("profileurl", profileurl);
	        jgen.writeNumberField("followers", followers);
	        jgen.writeNumberField("following", following);
	        jgen.writeEndObject();	
			
		}}

		@Override
		public String toString() {
			super.toString();
			ObjectMapper mapper = new ObjectMapper();
			SimpleModule module = new SimpleModule();
			module.addSerializer(ProfileDTO.class,new IntervalSerializer());
			mapper.registerModule(module);
			try {
				return mapper.writeValueAsString(this);
			} catch (com.fasterxml.jackson.core.JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
//		@Override
//		public ProfileDTO deserialize(JsonParser jsonParser, com.fasterxml.jackson.databind.DeserializationContext ctxt)
//				throws IOException, JsonProcessingException {
//			  ProfileDTO instance = new ProfileDTO();
//	           
//	            if (jsonParser.getCurrentToken() == null) {
//	                jsonParser.nextToken();
//	            }
//	            if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
//	                jsonParser.skipChildren();
//	                return null;
//	            }
//	            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
//	                String fieldName = jsonParser.getCurrentName();
//	                jsonParser.nextToken();
//	                instance.parseField(instance, fieldName, jsonParser);
//	                jsonParser.skipChildren();
//	            }
//	            return instance;
//		}
		
//		public String process() {
//			ProfileDTO profiledto = LoginDataBase.getProfileDataFromdatabase(1);
//			return profiledto.toString();
//		}
		
		
}
