package requestresponse;

public interface ResponseBase {
	ResponseBase process();
	void  postProcess();
	String responseInJson();

}

