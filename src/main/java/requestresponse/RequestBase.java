package requestresponse;


public interface RequestBase{

	boolean preProcess();
	String process();
}

