package requestresponse;

import datatransferobject.ProfileDTO;
import requestresponse.LoginRequest.Builder;

public abstract class AbstractRequest  implements RequestBase{
	private RequestType requestType;
	
	public AbstractRequest(Builder b){
		this.requestType = b.requestType;
	}
	public AbstractRequest(RequestType requestType){
		this.requestType = requestType;
	}
	public static class Builder<E extends RequestBase> {
		private RequestType requestType;
		
		public Builder profileDto(RequestType requestType) {
			this.requestType = requestType;
			return this;
		}
		
	}
}
