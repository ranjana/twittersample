package requestresponse;

public enum RequestType {
	   LOGIN(1) {
			@Override
			public String requestName() {
				return "get_login_request";
			}
		},
		APIErrorTwo(2) {
				@Override
				 public String requestName() {
					return null;
				}
			},
		APIErrorThree(3) {
				@Override
				public String requestName() {
					return null;
				}
	     },
	APIErrorFour(4) {
				@Override
				public String requestName() {
					return null;
				}
			};

		    private int code;

		    private RequestType(int code) {
		        this.code = code;
		    }

		    public int getCode() {
		        return this.code;
		    }
		    
		  public  abstract String requestName();
		
}
