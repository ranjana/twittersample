package requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import datatransferobject.ProfileDTO;

public class LoginResponse extends JsonDeserializer<LoginResponse>{
	private ProfileDTO profileDto;
	private boolean shouldshowdetailsPageOnFrontend; 
	
	public LoginResponse(Builder b) {
		this.profileDto =b.profileDto;
		this.shouldshowdetailsPageOnFrontend =b.shouldshowdetailsPageOnFrontend;
	}
	public LoginResponse() {
//		    ObjectMapper mapper = new ObjectMapper();
//			SimpleModule module = new SimpleModule();
//			module.addDeserializer(LoginRequest.class,new CustomDeserilizer());
//			mapper.registerModule(module);
	}
	
	public static class Builder{
		private ProfileDTO profileDto;
		private boolean shouldshowdetailsPageOnFrontend;
		
		public Builder profileDto(ProfileDTO profileDto) {
			this.profileDto = profileDto;
			return this;
		}
		
		public Builder shouldshowdetailsPageOnFrontend(boolean shouldshowdetailsPageOnFrontend) {
			this.shouldshowdetailsPageOnFrontend = shouldshowdetailsPageOnFrontend;
			return this;
		}
		
		public LoginResponse build() {
			return new LoginResponse(this);
		}
		
	}

	public void parseField(LoginResponse instance, String fieldName, JsonParser p) throws IOException{
		// TODO Auto-generated method stub
		switch (fieldName) {
		case "profileDto":
       p.nextToken();
       profileDto = p.readValueAs(ProfileDTO.class);
       break;
		case "shouldshowdetailsPageOnFrontend":
		       p.nextToken();
		       shouldshowdetailsPageOnFrontend = p.readValueAs(boolean.class);
		       break;
		}
	}
	
	public class IntervalSerializer extends JsonSerializer {
		@Override
		public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {		// TODO Auto-generated method stub
		jgen.writeStartObject();
        jgen.writeObjectField("profileDto",profileDto);
        jgen.writeBooleanField("shouldshowdetailsPageOnFrontend",shouldshowdetailsPageOnFrontend);
        jgen.writeEndObject();
		
	}
	}
	@Override
	public String toString() {
		super.toString();
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(LoginResponse.class,new IntervalSerializer());
		mapper.registerModule(module);
		try {
			return mapper.writeValueAsString(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
		 public LoginResponse deserialize(JsonParser jsonParser, com.fasterxml.jackson.databind.DeserializationContext ctxt)
	                throws IOException {
	            final LoginResponse instance = new LoginResponse();
	           
	            if (jsonParser.getCurrentToken() == null) {
	                jsonParser.nextToken();
	            }
	            if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
	                jsonParser.skipChildren();
	                return null;
	            }
	            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
	                String fieldName = jsonParser.getCurrentName();
	                jsonParser.nextToken();
	                instance.parseField(instance, fieldName, jsonParser);
	                jsonParser.skipChildren();
	            }
	            return instance;
	           }

	}

