package requestresponse;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import DBHelper.LoginDataBase;
import datatransferobject.ProfileDTO;

/** this request is for both login and profile details **/

public class LoginRequest implements RequestBase{
	private boolean isloginRequest;
	private ProfileDTO profileDto;
	
	public LoginRequest(Builder b) {
		this.profileDto =b.profileDto;
		this.isloginRequest = b.isloginRequest;
	}
	public LoginRequest() {
//		    ObjectMapper mapper = new ObjectMapper();
//			SimpleModule module = new SimpleModule();
//			module.addDeserializer(LoginRequest.class,new CustomDeserilizer());
//			mapper.registerModule(module);
	}
	
	public static class Builder{
		private ProfileDTO profileDto;
		private Boolean isloginRequest;
		
		public Builder profileDto(ProfileDTO profileDto) {
			this.profileDto = profileDto;
			this.isloginRequest = isloginRequest;
			return this;
		}

		
	}
//	
//	public static class CustomDeserilizer extends JsonDeserializer<LoginRequest> {
//        @Override
//        public LoginRequest deserialize(JsonParser jsonParser, com.fasterxml.jackson.databind.DeserializationContext ctxt)
//                throws IOException {
//            final LoginRequest instance = new LoginRequest();
//           
//            if (jsonParser.getCurrentToken() == null) {
//                jsonParser.nextToken();
//            }
//            if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
//                jsonParser.skipChildren();
//                return null;
//            }
//            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
//                String fieldName = jsonParser.getCurrentName();
//                jsonParser.nextToken();
//                instance.parseField(instance, fieldName, jsonParser);
//                jsonParser.skipChildren();
//            }
//            return instance;
//           }
//	}

	public void parseField(LoginRequest instance, String fieldName, JsonParser p) throws IOException{
		// TODO Auto-generated method stub
		switch (fieldName) {
		case "profileDto":
        p.nextToken();
        profileDto = p.readValueAs(ProfileDTO.class);
        break;
		case "isloginRequest":
	    p.nextToken();
	    isloginRequest = p.readValueAs(boolean.class);
	    break;
		}
	}
	
	public class IntervalSerializer extends JsonSerializer {
		@Override
		public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {		// TODO Auto-generated method stub
		jgen.writeStartObject();
        jgen.writeObjectField("profileDto",profileDto);
        jgen.writeBooleanField("isloginRequest",isloginRequest);

        jgen.writeEndObject();
		
	}
	}
	@Override
	public String toString() {
		super.toString();
		try {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(LoginRequest.class,new IntervalSerializer());
		mapper.registerModule(module);
			return mapper.writeValueAsString(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public LoginRequest deserialize(JsonParser jsonParser, com.fasterxml.jackson.databind.DeserializationContext ctxt)
	                throws IOException {
	            final LoginRequest instance = new LoginRequest();
	           
	            if (jsonParser.getCurrentToken() == null) {
	                jsonParser.nextToken();
	            }
	            if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
	                jsonParser.skipChildren();
	                return null;
	            }
	            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
	                String fieldName = jsonParser.getCurrentName();
	                jsonParser.nextToken();
	                instance.parseField(instance, fieldName, jsonParser);
	                jsonParser.skipChildren();
	            }
	            return instance;
	           }
	
	@Override
	public boolean preProcess() {
		// TODO Auto-generated method stub
		//!isloginRequest = signupform coming
		if(!isloginRequest) {
			LoginDataBase.updateProfile(profileDto);
		}else {
			LoginDataBase.createProfile(profileDto);
		}
		return false;
	}
	@Override
	public String process() {
		LoginResponse.Builder resposne = new LoginResponse.Builder();
		ProfileDTO profiledto = LoginDataBase.getProfileDataFromdatabase("jmsranjana.95@gmail.com");
		if(profiledto != null && profiledto.getFirstName() != null && profiledto.getLastName() != null ) {
			return resposne.profileDto(profiledto).shouldshowdetailsPageOnFrontend(false).build().toString();

		}
		return resposne.profileDto(profiledto).shouldshowdetailsPageOnFrontend(true).build().toString();
	}
	}
