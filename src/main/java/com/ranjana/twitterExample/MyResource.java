package com.ranjana.twitterExample;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import datatransferobject.ProfileDTO;
import requestresponse.LoginRequest;
import requestresponse.LoginResponse;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
	 @GET 
	 @Path("login")
	 @Produces(MediaType.TEXT_PLAIN)
	    public String getIt() {
	        return "Hi there!";
	    }
	 @POST
	 @Path("profile")
//	 @Produces(MediaType.APPLICATION_JSON)
	 public String profileDto() {
//	 	Person p = new Person();
//	 	p.setFirstName("ranjana");
//	 	p.setMiddleName("gupta");
//	 	p.setLastName("gupta");
	 	LoginRequest p = new LoginRequest();
	 	System.out.println("person test");	
	 	return p.process();
	 			
	 }
    @POST
    @Path("loginrequest")
//    @Produces(MediaType.APPLICATION_JSON)
    	public String getusers(ProfileDTO profiledto) {
    		System.out.println("start of user resourse........");
    		LoginRequest.Builder builder = new LoginRequest.Builder();
    		builder.profileDto(profiledto);
    		LoginRequest req =new LoginRequest(builder);
//    		return "{ name: \"John\", age: 30, city: \"New York\" }";
    		return req.process().toString();    		
    	}
    
//    @GET
//    @Path("getProfile/{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    	public ProfileDTO getuser(@PathParam("id") int id) {
//    		System.out.println("start of getuser by id user resourse........");
//    		LoginRequest.Builder builder = new LoginRequest.Builder();
//    		builder.profileDto(profiledto);
//    		LoginRequest req =new LoginRequest(builder);
//    		return req.process();
//    }
}
