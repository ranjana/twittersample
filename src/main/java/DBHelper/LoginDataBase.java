package DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import connection.Databaseconnection;
import datatransferobject.ProfileDTO;
import datatransferobject.ProfileDTO.Builder;

public class LoginDataBase{
	
	static Connection con = null;
	private static String PROFILE_TABLE_NAME = "userProfile";
	private static String PROFILE_TABLE = "CREATE TABLE IF NOT EXISTS" +PROFILE_TABLE_NAME+" ("
			+ "id int(5) NOT NULL AUTO_INCREMENT,"
			+ " user_id varchar(50) NOT NULL, PRIMARY KEY(id),"
			+ "  unique key(user_id),"
			+ " firstName varchar(50) DEFAULT NULL,"
			+ "lastName varchar(50) DEFAULT NULL,"
			+ "age int(3) DEFAULT 0,"
			+ " profileurl varchar(300) DEFAULT NULL,"
			+ "followers int(3) DEFAULT 0,"
			+ "following int(3) DEFAULT 0"
			+ ");";
	public static void createTable() {
		con = Databaseconnection.getInstance();
		try {
			Statement stmnt =con.createStatement();
			stmnt.executeUpdate(PROFILE_TABLE);
			System.out.println("PROFILE_TABLE created successfully");
		} catch (SQLException e) {
			System.out.println("error in creating PROFILE_TABLE");
			e.printStackTrace();
		}
		
	}

	
	public static boolean createProfile(ProfileDTO profiledto) {
//		con = Databaseconnection.getInstance();
		createTable();
		String sql = "";
		 sql = "INSERT INTO userProfile  VALUES ( null,?,?,?,?,?,?,? )";
//		String sql = "INSERT INTO users VALUES (null,?,?,?)"; 
	
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, profiledto.userid);
			ps.setString(2,profiledto.firstName);
			ps.setString(3,profiledto.lastName);
			ps.setLong(4, profiledto.age);
			ps.setString(5,profiledto.profileurl);
			ps.setLong(6,profiledto.followers);
			ps.setLong(7,profiledto.following);
			ps.executeUpdate();
			System.out.println("profile dto successfully created");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean updateProfile(ProfileDTO profiledto) {
		createTable();
//		con = Databaseconnection.getInstance();
		String sql = "UPDATE INTO userProfile  VALUES ( null,?,?,?,?,?,?,? )";
//		String sql = "INSERT INTO users VALUES (null,?,?,?)"; 
	
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, profiledto.userid);
			ps.setString(2,profiledto.firstName);
			ps.setString(3,profiledto.lastName);
			ps.setLong(4, profiledto.age);
			ps.setString(5,profiledto.profileurl);
			ps.setLong(6,profiledto.followers);
			ps.setLong(7,profiledto.following);
			ps.executeUpdate();
			System.out.println("profile dto successfully updated");
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static ProfileDTO getProfileDataFromdatabase(String userId) {
			con = Databaseconnection.getInstance();
			String sql ="select *  from userProfile where user_id = \""+userId+"\";";
//			String sql = "select *  from " + PROFILE_TABLE_NAME+" where id ="+userid;
			System.out.println("start of getuser getUserById........");
			try {
				System.out.println("start of getuser try........");
				ProfileDTO.Builder profileDto = new ProfileDTO.Builder();
				Statement st= con.createStatement();
				ResultSet rs = st.executeQuery(sql);
				System.out.println("start of executeQuery........");
				if(rs.next()) {
					profileDto.userId(rs.getString("user_id"))
					.firstName(rs.getString("firstName"))
					.lastName(rs.getString("lastName"))
					.age(rs.getInt("age"))
					.profileUrl(rs.getString("profileurl"))
					.followers(rs.getLong("followers"))
					.following(rs.getLong("following"));
					return profileDto.build();
			
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{ 
	            System.out.println("finally block of get user by id executed"); 
	        }
		
		return null;
	}

}
